# Titanic Survivivability via use of Neural Nets

First of all big thanks go to jaza10 at github where this code originates from (https://github.com/jaza10/AppliedNeuralNetworkTitanicSurvival). I had written a Titanic NN example previously, but unfortunately lost it, this project came to rescue.

That being said, the project code has been manually forked and converted to use Keras and live loss plots for demonstration purposes.
